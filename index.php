<?php

$loader = new Phalcon\Loader();
$loader->registerDirs(['.']);
$loader->register();

$di = new Phalcon\Di\FactoryDefault();

$di->set(
    'db',
    function () {
        return new Phalcon\Db\Adapter\Pdo\Mysql( ['host' => 'localhost', 'username' => 'root', 'dbname' => 'kadai']);
    }
);

$di->set(
    'view',
    function () {
        $view = new Phalcon\Mvc\View();
        $view->setViewsDir('.');
        return $view;
    }
);

$di->set(
    'url',
    function () {
        $url = new Phalcon\Mvc\Url();
        $url->setBaseUri('/');
        return $url;
    }
);

$di->set(
    'router',
    function(){
        $router = new Phalcon\Mvc\Router(false);
        $router->removeExtraSlashes(true);
        $router->addGet('/products', ['controller' => 'products', 'action' => 'index']);
        $router->addPost('/products', ['controller' => 'products', 'action' => 'add']);
        $router->addGet('/products/search', ['controller' => 'products', 'action' => 'search']);
        $router->addGet('/products/{id:\d+}', ['controller' => 'products', 'action' => 'get']);
        $router->addPut('/products/{id:\d+}', ['controller' => 'products', 'action' => 'update']);
        $router->addDelete('/products/{id:\d+}', ['controller' => 'products', 'action' => 'delete']);

        $router->addPost('/images', ['controller' => 'images', 'action' => 'add']);
        $router->addGet('/images/{filename:[0-9a-fA-F]{64}}', ['controller' => 'images', 'action' => 'get']);
        $router->addDelete('/images/{filename:[0-9a-fA-F]{64}}', ['controller' => 'images', 'action' => 'delete']);

        $router->notFound(['controller' => 'notFound', 'action' => 'notFound']);
        return $router;
    }
);

$router = $di['router'];
$router->handle();

$dispatcher = $di['dispatcher'];
$dispatcher->setControllerName($router->getControllerName());
$dispatcher->setActionName($router->getActionName());
$dispatcher->setParams($router->getParams());

try{
    $dispatcher->dispatch();
} catch (Exception $e) {
    $response = new Phalcon\Http\Response();
    $response->setContentType('application/json', 'UTF-8');
    $response->setStatusCode(500);
    $response->setContent(json_encode(null));
    $response->send();
    die();
}

$response = $dispatcher->getReturnedValue();

if ($response instanceof Phalcon\Http\ResponseInterface) {
    $response->send();
}
