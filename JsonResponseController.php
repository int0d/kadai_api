<?php

// JSONで結果返すための基底クラス

class JsonResponseController extends Phalcon\Mvc\Controller {
    
    function jsonResponse($data, $code=200){
        $response = new \Phalcon\Http\Response();
        $response->setStatusCode($code);
        #if(isset($data)){
        $response->setContentType('application/json', 'UTF-8');
        $response->setContent(json_encode($data));
        #}
        return $response;
    }
}