<?php

class NotFoundController extends JsonResponseController {
    // 404
    public function notFoundAction(){
        return $this->jsonResponse(['errors' => ['message'=>'アクションが見つかりません']], 404);
    }
}